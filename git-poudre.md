## La Poudrière !

Pour ceux qui l'ignore c'est notre réserve à munitions pour XP mais surtout, pour les OP.
[pre]  
État du stock [time date=white time=white timer=gray]2018-08-16 03:03:12 CEST[/time]
État du stock : jeu. 16 août 2018 03:03:12 CEST

| Quantité | Matière                 | Pays            |
| ---      | ---                     | ---             |
|  450     | Écorce Mitexi de Choix  | Forêts q250     |
|  318     | Huile Koorin de Choix   | Forêts q250     |
|  417     | Huile Gulatch de Choix  | Forêts q250     |
|  446     | Résine Colle de Choix   | Forêts q250     |
|          |                         |                 |
|  171     | Écorce Mitexi de Choix  | Jungle q250     |
|  81      | Huile Gulatch de Choix  | Jungle q250     |
|  211     | Résine Colle de Choix   | Jungle q250     |
|  513     | Résine Fung de Choix    | Jungle q250     |
|          |                         |                 |
|  46      | Écorce Beckers de Choix | Lacs q250       |
| >999     | Écorce Beckers de Choix | Lacs q250       |
| >999     | Écorce Beckers de Choix | Lacs q250       |
|  895     | Huile Koorin de Choix   | Lacs q250       |
|  759     | Résine Colle de Choix   | Lacs q250       |
| >999     | Résine Colle de Choix   | Lacs q250       |
|          |                         |                 |
|  400     | Écorce Beckers de Choix | P. Racines q250 |
|  60      | Résine Colle Excellente | P. Racines q250 |

[/pre]
